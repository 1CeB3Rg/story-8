from django.test import TestCase,Client
from django.urls import resolve
from .views import home

class story8test(TestCase):
    def test_url_exist(self):
            response= Client().get('/')
            self.assertEqual(response.status_code,200)
    def test_using_func(self):
            found=resolve('/')
            self.assertEqual(found.func,home)
    def test_home_using_template(self):
            response = Client().get('/')
            self.assertTemplateUsed(response, 'home.html')